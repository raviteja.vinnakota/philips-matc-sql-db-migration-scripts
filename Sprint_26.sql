UPDATE users SET userpassword= 'Admin@123'

INSERT [dbo].[EmailTemplates] ([FromEmail], [TemplateName], [Template], [Subject], [IsReminderEmail], [DaysToRemind], [UpdatedBy], [UpdatedOn], [IsDeleted]) VALUES ( N'noreply@philips.com', N'SimpleEdit', N'<!DOCTYPE html>  
<head>  
<title>Opportunity Due</title>  
</head>  
<body> 
<br/>  Hi <<UserName>>,  
<br><br><br>  Opportunity: <<OpportunityTitle>> 
<br>Modified Stage: <<Stage>> 
<p>Opportunity <a href="<<URL>>"><<OpportunityId>></a> updated successfully.</p>
<br><br> <p>Please do not reply to this message. This is an auto-generated email service.<br>________________________________________________________________<br>This information contained in this message may be confidential and legally protected under applicable law. This message is intended solely for the addressee(s). If you are not the intended recipient, you are hereby notified that any use, forwarding, dissemination, or reproduction of this message is strictly prohibited and may be unlawful. If you are not the intended recipient, please contact the sender by return e-mail and destroy all copies of the original messages.</p>    
</body>    
</html>', N'Opportunity <<OpportunityId>> updated successfully', 1, 1, 1, CAST(N'2021-08-30T15:47:02.990' AS DateTime), 0)

