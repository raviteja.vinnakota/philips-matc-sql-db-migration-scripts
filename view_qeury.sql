USE [VASWMS_Prodb_10112022]
GO

/****** Object:  View [dbo].[ViewSearchResults]    Script Date: 2/21/2023 3:22:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************************************************************
FileName : ViewSearchResults.sql
Date: 25-08-2021
******************************************************************************************************************************/

ALTER VIEW [dbo].[ViewSearchResults]
AS
  SELECT opportunities.opportunity_id                           AS OpportunityId
         ,
         title                                                  AS
         Title,
         Stuff((SELECT DISTINCT ',' + Cast(owner AS VARCHAR(10)) [text()]
                FROM   opportunity_owner WITH(nolock)
                WHERE  opportunity_owner.opportunity_id =
                       opportunities.opportunity_id
                       AND opportunity_owner.approver_decision = 0
                FOR xml path('')), 1, 1, '')                    OwnersId,
         Stuff((SELECT DISTINCT ',' + OwnerUser.firstname + ' '
                                + OwnerUser.lastname + '(' + OwnerUser.ldapid +
                                ')'
                FROM   opportunity_owner WITH(nolock)
                       LEFT JOIN users AS OwnerUser WITH(nolock)
                              ON OwnerUser.id = opportunity_owner.owner
                WHERE  opportunity_owner.opportunity_id =
                       opportunities.opportunity_id
                       AND opportunity_owner.approver_decision = 0
                FOR xml path('')), 1, 1, '')                    Owners,
         opportunities.stage                                    AS StageId,
         workflow_steps.step_name                               AS Stage,
         status                                                 AS StatusId,
         CASE
           WHEN status = 1 THEN 'On Track'
           WHEN status = 2 THEN 'Attention'
           WHEN status = 3 THEN 'OverDue'
		   WHEN status = 4 THEN 'Completed'
		   WHEN status = 5 THEN 'Removed'
         END                                                    AS Status,
         opportunities.current_step                             AS CurrentStep,
         opportunities.currentstep_startdate                    AS
            CurrentStepStartDate,
         opportunities.approver_decision                        AS
            ApproverDecisionId,
         CASE
           WHEN opportunities.approver_decision = 1 THEN 'Approved'
           WHEN opportunities.approver_decision = 2 THEN 'Rejected'
           WHEN opportunities.approver_decision = 3 THEN 'RequestToChange'
           WHEN opportunities.approver_decision = 4 THEN 'Not Completed'
         END                                                    AS
            ApproverDecision,
         opportunities.created_date                             AS CreatedDate,
         opportunities.updated_date                             AS LastUpdated,
         opportunities.created_by                               AS CreatedById,
         CreatedByUsers.firstname + ' '
         + CreatedByUsers.lastname + '('
         + CreatedByUsers.ldapid + ')'                          AS CreatedBy,
         Isnull(pn_performtesting.part_number, '')              AS PhilipsPartNo
         ,
         Isnull(pn_performtesting.manf_partnumber, '')          AS
            ManufacturerPartNo,
         Isnull(pn_performtesting.part_description, '')         AS
         PhilipsPartDesc
            ,
         Isnull(pn_performtesting.manf_partdescription, '')     AS
            ManufacturerPartDesc,
         Isnull(pn_performtesting.reference_psa_partnumber, '') AS
            ReferencePSAPartNo,
         opportunities.is_deleted                               AS IsDeleted,
         opportunities.description                              AS Description,
         opportunities.Reason,
         viewopportunityapprovers.*,
		CASE
           WHEN ViewLaunchSolutionWorkflowDetails.IsContractRequired = 1 THEN 'Yes'
           WHEN ViewLaunchSolutionWorkflowDetails.IsContractRequired = 0 THEN 'No'
           ELSE ''
         END as IsContractRequired,
		ViewLaunchSolutionWorkflowDetails.VendorManagementAuthorizationRequired,
		CASE
           WHEN ViewLaunchSolutionWorkflowDetails.FinanceDirectirApproval = 1 THEN 'Yes'
		   WHEN ViewLaunchSolutionWorkflowDetails.FinanceDirectirApproval = 0 THEN 'No'
		   ELSE ''
		END as FinanceDirectirApproval,
		CASE
           WHEN ViewLaunchSolutionWorkflowDetails.ProductDirectorApproval = 1 THEN 'Yes'
		   WHEN ViewLaunchSolutionWorkflowDetails.ProductDirectorApproval = 0 THEN 'No'
		   ELSE ''
		END as ProductDirectorApproval,
		CASE
           WHEN ViewLaunchSolutionWorkflowDetails.SDCApproval = 1 THEN 'Yes'
		   WHEN ViewLaunchSolutionWorkflowDetails.SDCApproval = 0 THEN 'No'
		   ELSE ''
		END as SDCApproval,
		ViewLaunchSolutionWorkflowDetails.PartNumbers,
		ViewLaunchSolutionWorkflowDetails.ManufacturePartNumbers,
		CASE
           WHEN ViewLaunchSolutionWorkflowDetails.PreloadOfAPTTUSCompleted = 1 THEN 'Yes'
		   WHEN ViewLaunchSolutionWorkflowDetails.PreloadOfAPTTUSCompleted = 0 THEN 'No'
		   ELSE ''
		END as PreloadOfAPTTUSCompleted,
		ViewLaunchSolutionWorkflowDetails.APTTUSReferenceNumber,
		ViewLaunchSolutionWorkflowDetails.SAPReferenceNumber,
		ViewLaunchSolutionWorkflowDetails.eDMRReferenceNumber,
		ViewLaunchSolutionWorkflowDetails.ReferencePSAPartNumbers,
		ViewLaunchSolutionWorkflowDetails.SAPReleaseReferenceNumber,
		ViewLaunchSolutionWorkflowDetails.SOFONReleaseReferenceNumber,
		CASE
           WHEN ViewLaunchSolutionWorkflowDetails.ReleaseToAPTTUSApproved = 1 THEN 'Yes'
		   WHEN ViewLaunchSolutionWorkflowDetails.ReleaseToAPTTUSApproved = 0 THEN 'No'
		   ELSE ''
		END as ReleaseToAPTTUSApproved,
		ViewLaunchSolutionWorkflowDetails.APTTUSReleaseReferenceNumber,
		f1.test_start_date AS ExpectedVAndVTestingStartDate,
		ViewLaunchSolutionWorkflowDetails.StartDate AS ActualVAndVTestingStartDate,
		Isnull([dbo].[GetBuisinessDays](f1.approval_date,f1.test_start_date),null)as EstimatedVAndVTestingDelay,
		Isnull([dbo].[GetBuisinessDays](f1.approval_date,ViewLaunchSolutionWorkflowDetails.StartDate),null)as ActualVAndVTestingDelay,
		opportunities.parent_id                           AS ParentId
       FROM   opportunities WITH(nolock)	   
LEFT JOIN opportunity_f1_form_approval f1 WITH(nolock)
ON opportunities.opportunity_id = f1.opportunity_id
INNER JOIN workflow_steps WITH(nolock)
        ON opportunities.stage = workflow_steps.id
LEFT JOIN pn_performtesting WITH(nolock)
       ON opportunities.opportunity_id = pn_performtesting.opportunity_id
          AND pn_performtesting.is_deleted = 0
INNER JOIN users CreatedByUsers WITH(nolock)
        ON opportunities.created_by = CreatedByUsers.id
INNER JOIN viewopportunityapprovers WITH(nolock)
        ON opportunities.opportunity_id =
           viewopportunityapprovers.opportunity_id
INNER JOIN viewlaunchsolutionworkflowdetails WITH(nolock)
        ON opportunities.opportunity_id =
           viewlaunchsolutionworkflowdetails.opportunityid
           where (opportunities.is_deleted=0 OR status = 5)

GO


