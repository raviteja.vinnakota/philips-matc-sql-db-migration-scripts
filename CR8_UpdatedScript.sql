USE VASWMS

UPDATE opportunities SET currentStep_startDate = DATEADD(DAY, -1,GETDATE()) where is_deleted= 0 and approver_decision <> 2  
  
UPDATE [opportunity_history]  SET updated_date = DATEADD(DAY, -2,GETDATE()) WHERE opportunity_id IN (
SELECT opportunity_id FROM [dbo].[opportunities] where is_deleted= 0 and approver_decision <> 2  
)

UPDATE attachments SET created_date =DATEADD(DAY, -2,GETDATE()) WHERE opportunity_id IN (
SELECT opportunity_id FROM [dbo].[opportunities] where is_deleted= 0 and approver_decision <> 2 
)

UPDATE opportunity_f1_form_approval SET created_date = DATEADD(DAY, -2,GETDATE()) , updated_date = DATEADD(DAY, -1,GETDATE()) WHERE opportunity_id IN (
SELECT opportunity_id FROM [dbo].[opportunities] where is_deleted= 0 and approver_decision <> 2 
)

UPDATE pn_performtesting SET created_date = DATEADD(DAY, -2,GETDATE()) , updated_date = DATEADD(DAY, -1,GETDATE()) WHERE opportunity_id IN (
SELECT opportunity_id FROM [dbo].[opportunities] where is_deleted= 0 and approver_decision <> 2 
)

UPDATE VandVTesting SET createddate = DATEADD(DAY, -2,GETDATE()) , updateddate = DATEADD(DAY, -1,GETDATE()) WHERE opportunityid IN (
SELECT opportunity_id FROM [dbo].[opportunities] where is_deleted= 0 and approver_decision <> 2 
)
